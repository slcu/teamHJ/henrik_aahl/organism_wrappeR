R wrapper (work-in-progress) for use of Organism, developed in the Jönsson group 
at SLCU. Code is primarily based on organism_python_wrapper 
(https://gitlab.com/slcu/teamHJ/niklas/organism_python_wrapper), written by N. 
Korsbo.  

Documentation in the making. 
