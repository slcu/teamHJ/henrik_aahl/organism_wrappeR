#!/usr/bin/env Rscript
setwd("/home/henrik/compbio/thesis/code/modelling/") # Set path to 
source("organism_wrapper.R")
source("models.R")

### Set paths
simulator.path    = "/home/henrik/Organism/bin/simulator"
model.path        = "models/example.model"
init.path         = "inits/example.init"
stoch.solver.path = "solvers/rk4.solver"
model.name        = "example"

### Set simulation parameters
t.end         = 100
no.prints     = 100
step.size     = 1e-3
output.format = 2

###############################################################################
### Define system parameters
###############################################################################
wp = 0.5
wD = 0.2
wd = 0.4

Cp = 0.2
Cv = 0.4
Ck = 0.5
Cn = 2.0
Cd = 0.1

cp = 0.5
cD = 2 * wD
cd = 0.2

Xp = .5
Xv = .4
Xk = .7
Xn = 2.0
Xd = 0.2

xp = .5
xD = .2
xd = .4

###############################################################################
### Setup init file
###############################################################################
init.solver.rk4("solvers/rk4.solver", t.start = 0, t.end = t.end, 
                no.prints = no.prints, step.size = step.size, 
                output.format = output.format)

###############################################################################
### Setup model file
###############################################################################
model.obj = init.model(model.name)

### Add topology
addTopo(model.obj) = topo.3d()

### Add species, with and without reactions
addSpecies(model.obj) = init.species("Sink")
addSpecies(model.obj) = init.species("aWUS")
X     = init.species("X") 
pX    = init.species("pX")
pwus  = init.species("pWUS")
clv3  = init.species("CLV3")
pclv3 = init.species("pCLV3")

# WUS signal
addReaction(pwus) = creationOne(index = model.obj@species$aWUS@index, rate = wp)
addReaction(pwus) = diffusionSimple(rate = wD)
addReaction(pwus) = degradationOne(rate = wd)
addReaction(pwus) = degradationTwo(index = model.obj@species$Sink@index, rate = wD)
addSpecies(model.obj) = pwus

# CLV3
addReaction(clv3) = creationOne(index = model.obj@species$pWUS@index, rate = Cp)
addReaction(clv3) = hill(act.ind = list(), rep.ind = model.obj@species$pWUS@index + 3L, vars = c(Cv, Ck, Cn))
addReaction(clv3) = degradationOne(rate = Cd)
addSpecies(model.obj) = clv3

# CLV3 signal
addReaction(pclv3) = creationOne(index = model.obj@species$pWUS@index, rate = cp)
addReaction(pclv3) = diffusionSimple(rate = cD)
addReaction(pclv3) = degradationOne(rate = cd)
addReaction(pclv3) = degradationTwo(index = model.obj@species$Sink@index, rate = cD)
addSpecies(model.obj) = pclv3

# X
addReaction(X) = creationZero(Xp)
addReaction(X) = hill(act.ind = list(), rep.ind = model.obj@species$pCLV3@index, vars = c(Xv, Xk, Xn))
addReaction(X) = degradationOne(rate = Xd)
addSpecies(model.obj) = X

# X signal
addReaction(pX) = creationOne(rate = Xp, index = model.obj@species$X@index)
addReaction(pX) = diffusionSimple(rate = xD)
addReaction(pX) = degradationOne(rate = xd)
addReaction(pX) = degradationTwo(index = model.obj@species$Sink@index, rate = xD)
addSpecies(model.obj) = pX

### Add global reactions
# No reactions

### Add neighborhoodRule
addNeighRule(model.obj) = neighborhoodDistanceSphere(index = 3) 

# Write model to file
write.model(model.obj, file.path = model.path)

###############################################################################
### Set up init file
###############################################################################
# Set parameters
no.cells    = 20
no.species  = model.obj@no.species
no.topcells = 2
radius      = 2.5

# Randomize initial values (not really necessary)
init.molecules = matrix(runif(no.species * no.cells), no.cells, no.species)
init           = cbind(seq(from = 0, length.out = no.cells, by = 4), 
                       rep(0, no.cells), rep(0, no.cells), 
                       rep(radius, no.cells), init.molecules)
colnames(init) = c("x", "y", "z", "r", names(model.obj@species))

# Set initial values of sink and anchor
init[, "Sink"]  = rep(0, no.cells); init[1, "Sink"] = 1 # Set sink into stem
init[, "aWUS"]  = rep(0, no.cells); init[no.cells:(no.cells - no.topcells + 1), "aWUS"] = 1 # Set wus domain
init[, "X"]     = rep(0, no.cells); init[1, "X"] = 1 # Set top
init[, "pX"]    = rep(0, no.cells)
init[, "pWUS"]  = rep(0, no.cells)
init[, "CLV3"]  = rep(0, no.cells)
init[, "pCLV3"] = rep(0, no.cells)

# Write to file
write.init(outfile = init.path, init)

###############################################################################
### Simulate system
###############################################################################
result.ss = simulate(simulator.path, model.path, init.path, det.solver.path,      
                     toR = TRUE, output.mode = 2, init.out = init.ss.path)
result    = simulate(simulator.path, model.path, init.ss.path,
                     stoch.solver.path, toR = TRUE,  output.mode = 2)
# 
colnames(result)[1:(model.obj@no.species + 8)] = c("iter", "t", "cell",
                                                   "nNeigh", "x", "y", "z", "r",
                                                   names(model.obj@species))

plot.molecule = function(x) plot(result[, "x"], result[, x], main = x)

par(mfrow = c(3, 2))
plot.molecule("aWUS")
plot.molecule("pWUS")
plot.molecule("CLV3")
plot.molecule("pCLV3")
plot.molecule("X")
plot.molecule("pX")
