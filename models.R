######## Model definition
source("reactions.R",  local = TRUE)
source("species.R",    local = TRUE)
source("topologies.R", local = TRUE)
source("neigh_rule.R", local = TRUE)

setClass(
  "model",
  representation(
    name           = "character",
    no.topo        = "integer",
    no.species     = "integer",
    no.reactions   = "integer",
    no.neigh.rules = "integer",
    topologies     = "list",
    species        = "list",
    reactions      = "list",
    neigh.rules    = "list"
  ),
  prototype(
    name           = character(1),
    no.topo        = integer(1),
    no.species     = integer(1),
    no.reactions   = integer(1),
    no.neigh.rules = integer(1),
    topologies     = list(),
    species        = list(),
    reactions      = list(),
    neigh.rules    = list()
  )
)

### TODO:
# write writing function

##########################################
# Set adder
setGeneric("setIndex<-", function(x, value)
  standardGeneric("setIndex<-"))
setReplaceMethod("setIndex", "species", function(x, value) {
  x@index = as.integer(value)
  x
})

setGeneric("addTopo<-",
           function(object, value, ...)
             StandardGeneric("addTopo<-"))
setMethod("addTopo<-", c("model", "topo"),
          function(object, value) {
            object@topologies = append(object@topologies, value)
            object@no.topo    = object@no.topo + 1L
            object
          })

setGeneric("addSpecies<-",
           function(object, value, ...)
             StandardGeneric("addSpecies<-"))
setMethod("addSpecies<-", c("model", "species"),
          function(object, value) {
            setIndex(value)   = sum(sapply(object@topologies, function(x) 
              as.integer(x@parameters[1])), na.rm = TRUE) + object@no.species + 
              object@no.reactions + object@no.neigh.rules -1L + 1L # Set index for species. C++ indexes from 0, we from 1.
            object@species    = append(object@species, value)
            names(object@species)[length(object@species)] = value@name
            object@no.species = object@no.species + 1L
            object
          })

setMethod("addReaction<-", c("model", "reaction"),
          function(object, value) {
            object@reactions = append(object@reactions, value)
            object@no.reactions = object@no.reactions + 1L
            object
          })

setGeneric("addNeighRule<-",
           function(object, value, ...)
             StandardGeneric("addNeighRule<-"))
setMethod("addNeighRule<-", c("model", "neigh.rule"),
          function(object, value) {
            object@neigh.rules = append(object@neigh.rules, value)
            object@no.neigh.rules = object@no.neigh.rules + 1L
            object
          })

init.model = function(name = "") {
  new("model", name = name)
}

write.model = function(model, file.path = stdout(), comments = TRUE) {
  if(comments){
    cat('######################################',       file = file.path)
    cat('\n### MODEL NAME AND SPECIFICATION',           file = file.path, append = TRUE)
    cat('\n######################################\n\n', file = file.path, append = TRUE)
  }
  cat(model@name, model@no.topo, model@no.species, model@no.reactions, 
      model@no.neigh.rules, "\n", sep = "\t", file = file.path, append = TRUE)
  
  if(comments){
    cat('\n######################################',   file = file.path, append = TRUE)
    cat('\n### TOPOLOGIES',                           file = file.path, append = TRUE)
    cat('\n######################################\n', file = file.path, append = TRUE)
  }  
  for(ii in model@topologies) {
    cat("\n", file = file.path, append = TRUE)
    cat(ii@name, ii@parameters, "\n", sep = "\t", file = file.path, 
        append = TRUE)
    # for(jj in ii@reactions) {
    # TODO: print.reactions()
    # }
    # for(jj in ii@comp.changes) {
    # TODO: print.comp.changes()
    # }
  }
  if(comments){
    cat('\n######################################',   file = file.path, append = TRUE)
    cat('\n### SPECIES',                              file = file.path, append = TRUE)
    cat('\n######################################\n', file = file.path, append = TRUE)
  }  
  
  for(ii in model@species) {
    cat("\n", file = file.path, append = TRUE)
    cat(ii@name, ii@index, ii@no.reactions, "\n", sep = "\t", file = file.path, append = TRUE)
    for(jj in ii@reactions) {
      cat(jj@name, jj@parameters, "\n", sep = "\t", file = file.path, append = TRUE)
      cat(jj@variables, sep = "\n", file = file.path, append = TRUE)
    }
  }
  if(comments){
    cat('\n######################################',   file = file.path, append = TRUE)
    cat('\n### REACTIONS',                            file = file.path, append = TRUE)
    cat('\n######################################\n', file = file.path, append = TRUE)
  }
  for(ii in model@reactions) {
    cat("\n", file = file.path, append = TRUE)
    cat(ii@name, ii@parameters, "\n", sep = "\t", file = file.path, append = TRUE)
    cat(ii@variables, sep = "\n", file = file.path, append = TRUE)
  }
  if(comments){
    
    cat('\n######################################',   file = file.path, append = TRUE)
    cat('\n### NEIGHBORHOOD RULES',                   file = file.path, append = TRUE)
    cat('\n######################################\n', file = file.path, append = TRUE)
  }
  for(ii in model@neigh.rules) {
    cat("\n", file = file.path, append = TRUE)
    cat(ii@name, ii@parameters, "\n", sep = "\t", file = file.path, append = TRUE)
    cat(ii@variables, sep = "\n", file = file.path, append = TRUE)
  }
}

