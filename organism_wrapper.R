### TODO
read.model     = function(file.path, model) {}
get.cost       = function(estimator_binary, model, solver, estimator, param = NULL) {}
param.from.opt = function(opt.file) {}
optimise       = function(optimiser.path, model, solver, estimator, optimiser, args) {}

# Prerequisites
require(snow)
source("solvers.R")

#' Clean out comments and empty lines from a set of lines. 
#'
#' @param file.object A set of character strings
#' @param .parallel Boolean signifier of parallel treatment
#' @param cl Cluster object for parallelisation
#'
#' @return Parsed file
#'
clean.file.parsing = function(file.object, .parallel = FALSE, cl = NULL) {
  if(!.parallel)
    file.object = sapply(file.object, function(string) 
      gsub(pattern = "#[^\\\n]*", replacement = "", x = string)) # Remove comments
  else
    file.object = parSapply(cl, file.object, function(string) 
      gsub(pattern = "#[^\\\n]*", replacement = "", x = string)) # Remove comments
  file.object = file.object[which(file.object != "")] # Remove empty lines
  file.object = trimws(file.object) # Trim trailing/leading whitespace
  file.object
}

#' Read initialisation file for Organism model into R
#'
#' @param file.path 
#'
#' @return data.frame containing init info
#'
read.init = function(file.path) {
  data = read.table(file.path, skip = 1, blank.lines.skip = TRUE, comment.char = "#")
  colnames(data)[1:4] = c("x","y","z","r") # this isn't universal, is it?
  data
}

#' Read a solver file for Organism into R
#'
#' @param file.path 
#' @param sep String separator. Default: Any whitespace
#'
#' @return List of strings defining solver file
#'
read.solver = function(file.path, sep = "\\s+") {
  data = readLines(file.path)
  data = clean.file.parsing(data)
  sapply(data, function(x) strsplit(x, sep))
}

#' Write initialisation file to file (or default: stdout)
#'
#' @param outfile Path to output file
#' @param init Matrix structure (not containing top rows), holding 
#' initialisation data
#'
#' @return Printed lines via writeLines
#'
write.init = function(outfile = stdout(), init) {
  data = apply(init, 1, function(x) paste(x, collapse = "\t"))
  data = c(paste0(nrow(init), "\t", ncol(init)), data)
  writeLines(data, con = outfile)
}

#' Write parameter set to file
#'
#' @param file.path 
#' @param params One or many parameter sets
#'
#' @return Printed lines via writeLines
#' 
write.params = function(file.path = stdout(), params) {
  if(is.null(dim(params)))
    writeLines(paste(params, collapse = "\t"), con = file.path)
  else
    writeLines(apply(params, 1, function(x) paste(x, collapse = "\t")), con = file.path)
}

#' Write solver data to file
#'
#' @param file.path 
#' @param solver Array of strings defining model
#'
#' @return Printed lines via writeLines
#' 
write.solver = function(file.path = stdout(), solver) {
  text = apply(solver, 1, function(x) paste(x, collapse = "\t"))
  writeLines(text, con = file.path)
}

#' Read Organism parameters into R
#'
#' @param file.path 
#'
#' @return data.frame containing parameter data on separate rows
#'
read.params = function(file.path, header = FALSE) {
  read.table(file.path, header = header)
}

# Data consisting of lines
newman2gnuplot = function(data, .parallel = FALSE, cl = NULL, sep = '\\s+') {
  # TODO: Finish this
  # if(.parallel)
  #   data = parSapply(cl, clean.file.parsing(data, .parallel = .parallel, cl), function(x) as.numeric(strsplit(x, sep)[[1]]))
  # else
  #   data = sapply(clean.file.parsing(data), function(x) as.numeric(strsplit(x, sep)[[1]]))
  # new.blocks = which(apply(data, 1, length) < 4)  
  print("newman2gnuplot not yet implemented. Returning unmodified data.")
  return(data)
}


#' Parse Organism result into R.
#'
#' @param data Organism output data
#' @param output.mode newman = 1, gnuplot = 2, both = 5 
#' @param .parallel Boolean: Should we parallise?
#' @param cl Cluster object
#' @param sep String separator
#'
#' @return data.frame of Organism output
#' 
result2array = function(data, output.mode = 2, .parallel = FALSE, cl = NULL, sep = '\\s+') {
  # Gnuplot mode
  if (output.mode == 2) {
    if(.parallel)
      data = parSapply(cl, clean.file.parsing(data, .parallel = .parallel, cl), 
                       function(x) as.numeric(strsplit(x, sep)[[1]]))
    else
      data = sapply(clean.file.parsing(data), function(x) as.numeric(strsplit(x, sep)[[1]]))
    data = as.data.frame(t(unname(data)))
    return(data)
  }
  # Newman mode
  else if (output.mode == 1) { 
    data = newman2gnuplot(data)
    return(data)
  }
}

#' Write input data to temporary file and use this for simulation
#'
#' @param var One of model, init, solver, params
#'
#' @return Path to input file or created temporary link
#'
parse.org.arg = function(var) {
  if(!is.character(var) && !is.null(var)) {
    name = deparse(substitute(params)) # Get variable name
    tmp = tempfile()
    eval(parse(text = paste0("write.", name, "(file.path = tmp, ", name, " = ", name, ")")))
    return(tmp)
  } else {
    return (var)
  }
}

# simulator = "~/projects/sum16_solver/MSB_WCK/simulator"
# model     = "~/projects/sum16_solver/MSB_WCK/models/WCK.model"
# init      = "~/projects/sum16_solver/MSB_WCK/init/WCK.init"
# solver    = "~/projects/sum16_solver/MSB_WCK/solvers/rkadapt"
# simulator = "~/compbio/thesis/software/organism/bin/simulator"
# model     = "models/simpleRadial1.model"
# init      = "inits/simpleRadial1.init"
# solver    = "solvers/rk4.solver"
# 
# simulator.path    = "~/Organism/bin/simulator"
# model.path        = "models/simpleRadial1.model"
# init.path         = "inits/simpleRadial1.init"
# init.ss.path      = "inits/simpleRadial1_ss.init"
# det.solver.path   = "solvers/rk4.solver"
# stoch.solver.path = "solvers/gillespie.solver"
# parameters = c(wp, wD, wd, ws, Cp, Cd)
# 

simulator.path    = "~/Organism/bin/simulator"
model.path        = "models/simpleRadial2.model"
init.path         = "inits/simpleRadial2.init"
init.ss.path      = "inits/simpleRadial2_ss.init"
det.solver.path   = "solvers/rk4.solver"
# stoch.solver.path = "solvers/ms.solver"
stoch.solver.path = "solvers/gillespie.solver"
model.name        = "simple2"



#' Simulate Organism run in R
#' 
#' @param simulator Path to simulator binary
#' @param model Path to model file, or model data
#' @param init Path to initialisation file, or initialisation data
#' @param solver Path to solver file, or solver data
#' @param params Path to parameter file, or parameter data
#' @param init.out Path to initialisation file created from simulation output. Default: no print.
#' @param out.file Path to output of data
#' @param toR Boolean: Redirect output into R?
#' @param output.mode Printing mode: newman = 1, gnuplot = 2, both = 5
#'
#' @return Output file or data.frame in R via result2array
#'
simulate = function(simulator, model, init, solver, params = NULL, init.out = NULL, out.file = FALSE, toR = FALSE, output.mode = 2) {
  # Parse the arguments
  # TODO: Add params, init.out, out.file (?)
  model  = parse.org.arg(model)
  init   = parse.org.arg(init)
  solver = parse.org.arg(solver)
  params = parse.org.arg(params)
  
  # Expand paths, or Organism gets grumpy
  simulator = path.expand(simulator)
  model     = path.expand(model)
  init      = path.expand(init)
  solver    = path.expand(solver)
    
  # Define the command
  args = c(model, init, solver,
           ifelse(is.null(params),   "", paste("-parameter_input", params)),
           ifelse(is.null(init.out), "", paste("-init_output",     init.out)))
  args = args[args != ""] # remove empty fields
  
  # Run simulation
  result = tryCatch({
    system2(simulator, args = args, stderr = NULL, stdout = ifelse(toR, toR, out.file))
  }, warning = function(warning) {
    # TODO: I don't really know how to implement error catches in R, but this 
    # seems to work somewhat.
    cat(paste0("Warning:   ", warning))
  }, error = function(error) {
    cat(paste0("Error:   " , error))
  },
  finally = {"Simulation done"})
  
  if (toR)
    return (result2array(result, output.mode = output.mode))
}
