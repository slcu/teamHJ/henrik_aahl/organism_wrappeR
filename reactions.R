INDEX_NOT_SET = NA_integer_

setClass(
  "reaction",
  representation(
    name       = "character",
    parameters = "integer",
    variables  = "numeric"
  )
)

###############################################################################
### Creation
###############################################################################
hill = function(act.ind = NULL,
                rep.ind = NULL,
                vars = runif(1 + (length(act.ind) + length(rep.ind)) * 2)) {
  no.params = as.integer(length(vars))
  no.act    = as.integer(length(act.ind))
  no.rep    = as.integer(length(rep.ind))
  
  new(
    "reaction",
    name       = "hill",
    parameters = c(no.params, 2L, no.act, no.rep),
    variables  = c(vars, as.integer(act.ind), as.integer(rep.ind))
  )
}

creationZero = function(rate = runif(1)) {
  new(
    "reaction",
    name       = "creationZero",
    parameters = c(1L, 0L),
    variables  = c(rate)
  )
}

creationOne = function(rate = runif(1), index) {
  new(
    "reaction",
    name       = "creationOne",
    parameters = c(1L, 1L, 1L),
    variables  = c(rate, as.integer(index))
  )
}

###############################################################################
### Degradation
###############################################################################
degradationOne = function(rate = runif(1)) {
  new(
    "reaction",
    name       = "degradationOne",
    parameters = c(1L, 0L),
    variables  = c(rate)
  )
}
degradationTwo = function(rate = runif(1), index) {
  new(
    "reaction",
    name       = "degradationTwo",
    parameters = c(1L, 1L, 1L),
    variables  = c(rate, index)
  )
}


###############################################################################
### Diffusion
###############################################################################
diffusionSimple = function(rate = runif(1)) {
  new(
    "reaction",
    name       = "diffusionSimple",
    parameters = c(1L, 0L),
    variables  = c(rate)
  )
}
